=================
Salt Vagrant Demo
=================

A Salt Demo using Vagrant.
Very useful as local developement environment.
Machines have shared directories to host.
You can confiugre OS of each machine on the top of the ``Vagrantfile``

Machines are created in network: ``192.168.50`` if you wish to modify network, you need to edit ``Vagrantfile`` and master/minion config files.

Instructions
============

Run the following commands in a terminal. Git, VirtualBox and Vagrant must
already be installed.

.. code-block:: bash

    git clone https://gitlab.payconiq.io/jan.gazda/salt-vagrant-demo.git
    cd salt-vagrant-demo
    vagrant plugin install vagrant-vbguest
    vagrant up


This will download a VirtualBox (CentOS by default) image and create three virtual
machines for you. One will be a Salt Master named `master` and two will be Salt
Minions named `minion1` and `minion2`.  The Salt Minions will point to the Salt
Master and the Minion's keys will already be accepted. Because the keys are
pre-generated and reside in the repo, please be sure to regenerate new keys if
you use this for production purposes.

You can then run the following commands to log into the Salt Master and begin
using Salt.

.. code-block:: bash

    vagrant ssh master
    sudo salt \* test.ping


It is useful to use ``vagrant snapshot push <machine>`` after intial setup.
Then you can revert any changes doing just ``vagrant snapshot pop <machine> --no-delete``


Shared directories
------------------
Directories shared from host to guest machines.

Master:
.. code-block:: bash

    HOST:".(repo dir)/saltstack/salt/" -> GUEST: "/srv/salt"
    HOST:".(repo dir)/saltstack/pillar/" -> GUEST: "/srv/pillar"


Minions:

.. code-block:: bash

    HOST: ".(repo dir)/shared_dir/" -> GUEST: "/_host"

